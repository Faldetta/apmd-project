#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
import pandas as pd
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


# In[2]:


with open("./data/dpc-covid19-ita-province.json") as f:
    prov = json.load(f)
prov = pd.DataFrame(prov)


# Il dataset risultante dalla seguente operazione contiene tutte le provincie ordinate per latitudine.

# In[3]:


prov = (
    prov.drop(
        columns=[
            "data",
            "stato",
            "codice_regione",
            "denominazione_regione",
            "codice_provincia",
            "sigla_provincia",
            "totale_casi",
            "note",
        ],
        axis="columns",
    )
    .groupby("denominazione_provincia")
    .max()
    .drop(
        index=[
            "Fuori Regione / Provincia Autonoma",
            "In fase di definizione",
            "In fase di definizione/aggiornamento",
        ],
        axis="index",
    )
)


# La seguente funzione inserisce gli archi in un grafo a partire da una lista in modo che il primo elemento sia collegato con tutti gli altri.

# In[4]:


def insert_edge_from_list(graph, node_list):
    node = node_list.pop(0)
    edge_list = []
    for neigh in node_list:
        edge_list.append((node, neigh))
    graph.add_edges_from(edge_list)
    return graph


# Questa funzione filtra il dataframe in funzione della distanza dalla provincia, queste sono tutte ricevute in input.

# In[5]:


def filter_df(data_frame, province_row, distance):
    return data_frame[
        (data_frame.lat >= province_row.lat - distance)
        & (data_frame.lat <= province_row.lat + distance)
        & (data_frame.long >= province_row.long - distance)
        & (data_frame.long <= province_row.long + distance)
    ]


# La seguente funzione costruisce un grafo a partire da da un dataframe composto da indice, una colonna `lat` ed una `long` in modo tale due righe del DF siano connesse solo se le coordinate di una rientrano in quelle dell'altra a meno di un valore $\pm d$ su `lat` **e** `long`.

# In[6]:


def build_graph_from_df(data_frame, distance):
    graph = nx.Graph()
    graph.add_nodes_from(data_frame.index)
    prov_cpy = data_frame.copy()
    for index, province in prov_cpy.iterrows():
        prov_lat_long = filter_df(prov_cpy, province, distance)
        insert_edge_from_list(graph, prov_lat_long.index.to_list())
        prov_cpy.drop(index=index, inplace=True)
    return graph


# Costruiamo il grafo P delle province.

# In[7]:


# %%timeit
P = build_graph_from_df(prov, 0.8)


# Creiamo il DF di 2000 numeri randomici da usare per il grafo Q.

# In[8]:


np.random.seed(6)
random_couples = np.random.uniform(low=[30, 10], high=[50, 20], size=(2000, 2))
random_couples = pd.DataFrame(random_couples)
random_couples.rename(columns={0: "lat", 1: "long"}, inplace=True)


# Costruiamo il grafo Q.

# In[9]:


# %%timeit
Q = build_graph_from_df(random_couples, 0.08)


# Definiamo una funzione che aggiunge i come peso degli archi di un grafo la distanza sul DF dei nodi che lo compongono.

# In[10]:


def add_weight(graph, data_frame):
    for edge in graph.edges():
        coord1 = (
            float(data_frame[data_frame.index == edge[0]].lat),
            float(data_frame[data_frame.index == edge[0]].long),
        )
        coord2 = (
            float(data_frame[data_frame.index == edge[1]].lat),
            float(data_frame[data_frame.index == edge[1]].long),
        )
        graph[edge[0]][edge[1]]["weight"] = np.sqrt(
            np.power(coord1[0] - coord2[0], 2) + np.power(coord1[1] - coord2[1], 2)
        )
    return graph


# Aggiungiamo i pesi a P.

# In[11]:


# %%timeit
add_weight(P, prov)


# Aggiungiamo i pesi a Q.

# In[12]:


# %%timeit
add_weight(Q, random_couples)


# Questa funzione conta il numero di nodi con grado pari e dispari.

# In[13]:


def count_by_degree(graph):
    even, odd = 0, 0
    for _, degree in graph.degree():
        if degree % 2 == 0:
            even += 1
        else:
            odd += 1
    return even, odd


# Tale funzione ci comunica se un arco è un ponte.

# In[14]:


def is_bridge(graph, edge):
    return (edge[0], edge[1]) in nx.bridges(graph, root=edge[0]) or (
        edge[1],
        edge[0],
    ) in nx.bridges(graph, root=edge[0])


# Questa funzione decide il nodo di dinizio di un cammino euleriano.

# In[15]:


def starting_node(graph, odd):
    if odd == 2:
        start = [v for v in graph if graph.degree(v) % 2 != 0][0]
    else:
        start = [v for v in graph if graph.degree(v) % 2 == 0][0]
    return start


# Questa funzione comunica se il grafo ha un cammino euleriano.

# In[16]:


def has_eulerian_walk(graph, odd):
    return (odd == 0 or odd == 2) and nx.is_connected(graph)


# Tale funzione serve lo scopo di decidere quale arco seguire per poter effettuare correttamente un cammino euleriano.

# In[17]:


def choose_edge(graph, node, neighbor_list):
    for neighbor in neighbor_list:
        if not is_bridge(graph, (node, neighbor)):
            return (node, neighbor)
    return (node, [neigh for neigh in neighbor_list][0])


# Questa funzione restituisce il cammino euleriano, se presente, `None` altrimenti.

# In[18]:


def eulerian_walk(graph):
    _, odd = count_by_degree(graph)
    graph_cpy = graph.copy()
    has_eulerian = has_eulerian_walk(graph_cpy, odd)
    if not has_eulerian:
        return None
    else:
        eulerian_walk = []
        node = starting_node(graph_cpy, odd)
        neighbors = graph_cpy[node]
        while len(neighbors) != 0:
            edge = choose_edge(graph_cpy, node, neighbors)
            if edge is None:
                neighbors = []
            else:
                eulerian_walk.append(edge)
                graph_cpy.remove_edge(edge[0], edge[1])
                node = edge[1]
                neighbors = graph_cpy[node]
        return eulerian_walk


# Cerchiamo il cammino euleriano su P.

# In[19]:


walk = eulerian_walk(P)
if walk is None:
    print("Non c'è cammino euleriano")
else:
    walk


# E cerchiamolo su Q.

# In[20]:


walk = eulerian_walk(Q)
if walk is None:
    print("Non c'è cammino euleriano")
else:
    walk


# Per poter calcolare la closeness abbiamo deciso di usare l'Indice di Lin.

# In[21]:


def closeness_centrality(graph):
    closeness = {}
    n_nodes_of_graph = graph.number_of_nodes()
    for node in graph.nodes():
        distances = list(nx.single_target_shortest_path_length(graph, node))
        n_nodes_of_component = len(distances)
        fareness = 0
        for distance in distances:
            fareness += distance[1]
        if fareness != 0:
            closeness.update(
                {
                    node: (np.power(n_nodes_of_component - 1, 2))
                    / ((n_nodes_of_graph - 1) * fareness)
                }
            )
        else:
            closeness.update({node: 0})
    return closeness


# Calcoliamo la closeness dei nodi di P.

# In[22]:


closeness_centrality(P)


# E poi calcoliamola per quelli di Q.

# In[23]:


closeness_centrality(Q)

